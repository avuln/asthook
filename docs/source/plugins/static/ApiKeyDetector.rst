api_keys
==========

Descrition
##########

Neural Network Based, Automatic API Key Detector

A Multilayer-Perceptron-based system, able to identify API Key strings with an accuracy of over 99%.

For technical details, check out my thesis (Automatic extraction of API Keys from Android applications) and, in particular, Chapter 3 of the work.

Automatic API Key detector was developed by https://github.com/alessandrodd

Usage
#####

.. code-block:: bash

  asthook <app> --api_keys <normal|full>

.. asciinema:: ApiKeyDetector.cast
  :preload:
